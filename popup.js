chrome.tabs.query(
  { active: true, windowId: chrome.windows.WINDOW_ID_CURRENT },
  function (tabs) {
    const { id: tabId } = tabs[0].url;

    var code = `document.querySelector('meta[name="page:nid"]').content`;
    // http://infoheap.com/chrome-extension-tutorial-access-dom/
    chrome.tabs.executeScript(tabId, { code }, function (result) {
      var ul = document.querySelector("ul#fedbookAdminPopupLinks");
      var li = document.createElement("li");
      li.innerHTML = "nid: " + result[0];
      ul.appendChild(li);
    });

    var code = `document.querySelector('meta[name="page:gid"]').content`;
    // http://infoheap.com/chrome-extension-tutorial-access-dom/
    chrome.tabs.executeScript(tabId, { code }, function (result) {
      var ul = document.querySelector("ul#fedbookAdminPopupLinks");
      var li = document.createElement("li");
      li.innerHTML = "gid: " + result[0];
      ul.appendChild(li);
    });
  }
);
